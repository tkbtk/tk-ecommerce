#!/bin/bash
array=(*/);
for module in "${array[@]}";
do
    path="$module"
    if [[ "$path" != *"gradle"* ]]; then
        echo "$module";
        cd $module;
        ../gradlew clean install
        cd .. # out module
    fi
done
