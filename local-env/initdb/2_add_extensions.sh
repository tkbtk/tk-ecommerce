#!/bin/sh
for DB in $(psql -U postgres -t -c "SELECT datname FROM pg_database WHERE datname IN ('product_service_db')"); do
  psql -U postgres -d $DB -c "CREATE EXTENSION IF NOT EXISTS pg_trgm"
done
