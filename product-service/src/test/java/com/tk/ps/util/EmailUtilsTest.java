package com.tk.ps.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmailUtilsTest {

    @Test
    public void testVerifyValidEmail() {
         assertTrue(EmailUtils.isValid("example@abc.com"));
    }

    @Test
    public void testVerifyNullEmail() {
        assertFalse(EmailUtils.isValid(null));
    }

    @Test
    public void testVerifyEmptyEmail() {
        assertFalse(EmailUtils.isValid("     "));
    }

    @Test
    public void testVerifyInvalidEmail() {
        assertFalse(EmailUtils.isValid("abc@"));
    }

    @Test
    public void testGetDomain() {
        String domain = EmailUtils.getDomain("example@abc.com");
        assertEquals("abc.com", domain);
    }

    @Test
    public void testGetDomainForInvalidEmail() {
        String domain = EmailUtils.getDomain("example");
        assertEquals("", domain);
    }
}
