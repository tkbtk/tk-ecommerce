begin;
INSERT INTO category(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('Clothe', 'All clothe category', 'system', now(), 'system', now());
INSERT INTO category(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('Computer', 'All computer item', 'system', now(), 'system', now());
INSERT INTO product(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('T-Shirt', 'A beautiful T-shirt', 'system', now(), 'system', now());
INSERT INTO product(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('Shirt', 'A beautiful shirt', 'system', now(), 'system', now());
INSERT INTO product(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('ACME PC Computer', 'A new computer', 'system', now(), 'system', now());
INSERT INTO product(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('ACME Laptop', 'A new laptop', 'system', now(), 'system', now());

INSERT INTO product_attribute(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('Size', 'Clothe Size', 'system', now(), 'system', now());

INSERT INTO product_attribute(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('Color', 'Clothe Color', 'system', now(), 'system', now());

INSERT INTO product_attribute(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('RAM', 'Machine RAM', 'system', now(), 'system', now());

INSERT INTO product_attribute(name, description, created_by, created_date, last_modified_by, last_modified_date)
VALUES ('CPU', 'Machine CPU', 'system', now(), 'system', now());

INSERT INTO product_price(product_id, price, created_by, created_date, last_modified_by, last_modified_date)
SELECT id, 1000000, 'sytem', now(), 'system', now()
from product
where name = 'ACME Laptop';

INSERT INTO product_price(product_id, price, created_by, created_date, last_modified_by, last_modified_date)
SELECT id, 1200000, 'sytem', now(), 'system', now()
from product
where name = 'ACME PC Computer';

INSERT INTO product_price(product_id, price, created_by, created_date, last_modified_by, last_modified_date)
SELECT id, 120000, 'sytem', now(), 'system', now()
from product
where name = 'Shirt';

INSERT INTO product_price(product_id, price, created_by, created_date, last_modified_by, last_modified_date)
SELECT id, 120000, 'sytem', now(), 'system', now()
from product
where name = 'T-Shirt';

INSERT INTO product_item(product_id, sku, image_url, created_by, created_date, last_modified_by, last_modified_date)
SELECT id, '2b407e16e74a7-4a9b-b49f-a4fa9a269421', '', 'system', now(), 'system', now()
from product
where name = 'ACME Laptop';


INSERT INTO product_item(product_id, sku, image_url, created_by, created_date, last_modified_by, last_modified_date)
SELECT id, 'e21e7774-d05b-417c-9a9d-7af0b3f7eda3', '', 'system', now(), 'system', now()
from product
where name = 'ACME PC Computer';

INSERT INTO product_item(product_id, sku, image_url, created_by, created_date, last_modified_by, last_modified_date)
SELECT id, '5568265e-3c24-4574-9d92-94dc71caa854', '', 'system', now(), 'system', now()
from product
where name = 'Shirt';

INSERT INTO product_item(product_id, sku, image_url, created_by, created_date, last_modified_by, last_modified_date)
SELECT id, '3e68b4c0-192d-4442-b9e8-03149712e1a8', '', 'system', now(), 'system', now()
from product
where name = 'T-Shirt';

INSERT INTO product_item_attribute(product_item_id, product_attribute_id, value, created_by, created_date,
                                   last_modified_by, last_modified_date)
select q1.id, q2.paid, 'I7', 'system', now(), 'system', now()
from (SELECT pi.id as id from product_item pi where sku = '2b407e16-74a7-4a9b-b49f-a4fa9a269421') q1,
     (select pa.id as paid from product_attribute pa where pa.name = 'CPU') q2;

INSERT INTO product_item_attribute(product_item_id, product_attribute_id, value, created_by, created_date,
                                   last_modified_by, last_modified_date)
select q1.id, q2.paid, '16G', 'system', now(), 'system', now()
from (SELECT pi.id as id from product_item pi where sku = '2b407e16-74a7-4a9b-b49f-a4fa9a269421') q1,
     (select pa.id as paid from product_attribute pa where pa.name = 'RAM') q2;


INSERT INTO product_item_attribute(product_item_id, product_attribute_id, value, created_by, created_date,
                                   last_modified_by, last_modified_date)
select q1.id, q2.paid, 'I9', 'system', now(), 'system', now()
from (SELECT pi.id as id from product_item pi where sku = 'e21e7774-d05b-417c-9a9d-7af0b3f7eda3') q1,
     (select pa.id as paid from product_attribute pa where pa.name = 'CPU') q2;

INSERT INTO product_item_attribute(product_item_id, product_attribute_id, value, created_by, created_date,
                                   last_modified_by, last_modified_date)
select q1.id, q2.paid, '32G', 'system', now(), 'system', now()
from (SELECT pi.id as id from product_item pi where sku = 'e21e7774-d05b-417c-9a9d-7af0b3f7eda3') q1,
     (select pa.id as paid from product_attribute pa where pa.name = 'RAM') q2;

INSERT INTO product_item_attribute(product_item_id, product_attribute_id, value, created_by, created_date,
                                   last_modified_by, last_modified_date)
select q1.id, q2.paid, 'XL', 'system', now(), 'system', now()
from (SELECT pi.id as id from product_item pi where sku = '3e68b4c0-192d-4442-b9e8-03149712e1a8') q1,
     (select pa.id as paid from product_attribute pa where pa.name = 'Size') q2;

INSERT INTO product_item_attribute(product_item_id, product_attribute_id, value, created_by, created_date,
                                   last_modified_by, last_modified_date)
select q1.id, q2.paid, 'Black', 'system', now(), 'system', now()
from (SELECT pi.id as id from product_item pi where sku = '3e68b4c0-192d-4442-b9e8-03149712e1a8') q1,
     (select pa.id as paid from product_attribute pa where pa.name = 'Color') q2;


INSERT INTO product_item_attribute(product_item_id, product_attribute_id, value, created_by, created_date,
                                   last_modified_by, last_modified_date)
select q1.id, q2.paid, 'XXL', 'system', now(), 'system', now()
from (SELECT pi.id as id from product_item pi where sku = '5568265e-3c24-4574-9d92-94dc71caa854') q1,
     (select pa.id as paid from product_attribute pa where pa.name = 'Size') q2;

INSERT INTO product_item_attribute(product_item_id, product_attribute_id, value, created_by, created_date,
                                   last_modified_by, last_modified_date)
select q1.id, q2.paid, 'Blue', 'system', now(), 'system', now()
from (SELECT pi.id as id from product_item pi where sku = '5568265e-3c24-4574-9d92-94dc71caa854') q1,
     (select pa.id as paid from product_attribute pa where pa.name = 'Color') q2;

INSERT INTO product_item_inventory(product_item_id, count, created_by, created_date, last_modified_by,
                                   last_modified_date)
select id, 100, 'system', now(), 'system', now()
from product_item;
INSERT INTO product_category(product_id, category_id, created_by, created_date, last_modified_by, last_modified_date)
SELECT p.id, c.id ,'system', now(), 'system', now() from (select id from product where name in ('ACME Laptop','ACME PC Computer')) p, (select id from category where name ='Computer') c;

INSERT INTO product_category(product_id, category_id, created_by, created_date, last_modified_by, last_modified_date)
SELECT p.id, c.id ,'system', now(), 'system', now() from (select id from product where name in ('Shirt','T-Shirt')) p, (select id from category where name ='Clothe') c;
commit;

