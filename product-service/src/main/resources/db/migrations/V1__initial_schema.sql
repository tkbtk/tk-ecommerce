CREATE TABLE persistent_audit_event
(
    event_id   BIGSERIAL primary key,
    principal  VARCHAR(50) NOT NULL,
    event_date TIMESTAMP,
    event_type VARCHAR(255)
);

CREATE TABLE persistent_audit_evt_data
(
    event_id BIGSERIAL    NOT NULL,
    name     VARCHAR(150) NOT NULL,
    value    VARCHAR(255)
);

CREATE INDEX idx_persistent_audit_event ON persistent_audit_event (principal, event_date);

CREATE INDEX idx_persistent_audit_evt_data ON persistent_audit_evt_data (event_id);

ALTER TABLE persistent_audit_evt_data
    ADD PRIMARY KEY (event_id, name);
ALTER TABLE persistent_audit_evt_data
    ADD CONSTRAINT fk_evt_pers_audit_evt_data FOREIGN KEY (event_id) REFERENCES
        persistent_audit_event (event_id);

CREATE TABLE product
(
    id                 SERIAL PRIMARY KEY,
    name               VARCHAR(255) NOT NULL UNIQUE,
    description        VARCHAR(255),
    created_by         VARCHAR(50)  NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50)  NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE product_attribute
(
    id                 SERIAL PRIMARY KEY,
    name               VARCHAR(255) NOT NULL UNIQUE,
    description        VARCHAR(255),
    created_by         VARCHAR(50)  NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50)  NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE product_item
(
    id                 SERIAL PRIMARY KEY,
    product_id         INTEGER      NOT NULL,
    sku                varchar(255) NOT NULL UNIQUE,
    image_url          VARCHAR(255),
    created_by         VARCHAR(50)  NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50)  NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);
CREATE TABLE product_item_inventory
(
    id                 SERIAL PRIMARY KEY,
    product_item_id    INTEGER     NOT NULL,
    count              INTEGER,
    created_by         VARCHAR(50) NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50) NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE product_item_attribute
(
    id                   SERIAL PRIMARY KEY,
    product_item_id      INTEGER     NOT NULL,
    product_attribute_id INTEGER     NOT NULL,
    value                VARCHAR(50) NOT NULL,
    created_by           VARCHAR(50) NOT NULL,
    created_date         TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by     VARCHAR(50) NOT NULL,
    last_modified_date   TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE product_price
(
    id                 SERIAL PRIMARY KEY,
    product_id    INTEGER     NOT NULL,
    price              NUMERIC     NOT NULL,
    created_by         VARCHAR(50) NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50) NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE product_price_history
(
    id                 SERIAL PRIMARY KEY,
    product_id    INTEGER     NOT NULL,
    old_price          NUMERIC     NOT NULL,
    price              NUMERIC     NOT NULL,
    created_by         VARCHAR(50) NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50) NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE category
(
    id                 SERIAL PRIMARY KEY,
    name               VARCHAR(255) NOT NULL UNIQUE,
    description        VARCHAR(255),
    created_by         VARCHAR(50)  NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50)  NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE product_category
(
    product_id         INTEGER     NOT NULL,
    category_id        INTEGER     NOT NULL,
    created_by         VARCHAR(50) NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50) NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

ALTER TABLE product_category
    ADD CONSTRAINT fk_product_category_category FOREIGN KEY (category_id) REFERENCES category (id);
ALTER TABLE product_category
    ADD CONSTRAINT fk_product_category_product FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE product_item
    ADD CONSTRAINT fk_product_item_product FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE product_item_inventory
    ADD CONSTRAINT fk_product_item_inventory_product_item FOREIGN KEY (product_item_id) REFERENCES product_item (id);
ALTER TABLE product_item_attribute
    ADD CONSTRAINT fk_product_item_attribute_product_item FOREIGN KEY (product_item_id) REFERENCES product_item (id);
ALTER TABLE product_item_attribute
    ADD CONSTRAINT fk_product_item_attribute_product_attribute FOREIGN KEY (product_attribute_id) REFERENCES product_attribute (id);
ALTER TABLE product_price
    ADD CONSTRAINT fk_product_price_product FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE product_price_history
    ADD CONSTRAINT fk_product_price_history_product FOREIGN KEY (product_id) REFERENCES product (id);

