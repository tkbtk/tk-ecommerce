package com.tk.ps.model.domain;

import com.tk.ps.domain.AbstractAuditingEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Entity
@Data
@Table(name = "product_item")
public class ProductItem extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String sku;

    @Column
    private String imageUrl;

    @ManyToOne(targetEntity = Product.class)
    @JoinColumn(name = "product_id")
    private Product product;

    @OneToOne(targetEntity = ProductItemInventory.class, mappedBy = "productItem")
    private ProductItemInventory productItemInventory;

    @OneToMany(targetEntity = ProductItemAttribute.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_item_id")
    private List<ProductItemAttribute> attributes;
}
