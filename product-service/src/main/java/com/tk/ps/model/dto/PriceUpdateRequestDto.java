package com.tk.ps.model.dto;

import lombok.Data;

import java.math.BigInteger;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Data
public class PriceUpdateRequestDto {

    private BigInteger newPrice;
}
