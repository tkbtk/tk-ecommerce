package com.tk.ps.model.domain;

import com.tk.ps.domain.AbstractAuditingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "product")
public class Product extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @Column
    private String description;

    @ManyToMany(targetEntity = Category.class)
    @JoinTable(name = "product_category", joinColumns = {
        @JoinColumn(name = "product_id", referencedColumnName = "id")
    }, inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
    private List<Category> categories;

    @OneToMany(targetEntity = ProductItem.class, mappedBy = "product", fetch = FetchType.EAGER)
    private List<ProductItem> items;

    @OneToOne(targetEntity = ProductPrice.class, mappedBy = "product")
    private ProductPrice price;
}
