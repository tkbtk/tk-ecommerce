package com.tk.ps.model.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
public class ProductItemDto extends AuditDto {

    private long id;

    private String sku;

    private String imageUrl;

    private List<ProductItemAttributeDto> attributes;
}
