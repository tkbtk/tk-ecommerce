package com.tk.ps.model.dto;

import lombok.Data;

import java.time.Instant;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
public class AuditDto {

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

}
