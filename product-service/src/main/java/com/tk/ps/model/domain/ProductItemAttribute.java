package com.tk.ps.model.domain;

import com.tk.ps.domain.AbstractAuditingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "product_item_attribute")
public class ProductItemAttribute extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(targetEntity = ProductAttribute.class)
    @JoinColumn(name = "product_attribute_id")
    private ProductAttribute productAttribute;

    @Column
    private String value;
}
