package com.tk.ps.model.dto;

import lombok.Data;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
public class CategoryDto extends AuditDto {

    private long id;

    private String name;

    private String description;
}
