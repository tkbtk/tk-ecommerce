package com.tk.ps.model.domain;

import com.tk.ps.domain.AbstractAuditingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table
public class ProductItemInventory extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(targetEntity = ProductItem.class)
    @JoinColumn(name = "product_item_id")
    private ProductItem productItem;

    private String count;
}
