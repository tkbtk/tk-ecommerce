package com.tk.ps.model.dto;

import lombok.Data;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Data
public class ProductDetailsDto {

    private long id;

    private String name;

    private String description;

    private String sku;

    private BigInteger price;

    private int inventory;

    private List<ProductItemAttributeDto> attributes;

}
