package com.tk.ps.model.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
public class ProductDto extends AuditDto{

    private long id;

    private String name;

    private String description;

    private int price;

    private List<ProductItemDto> items;
}
