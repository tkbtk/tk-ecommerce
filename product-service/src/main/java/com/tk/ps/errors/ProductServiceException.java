package com.tk.ps.errors;

import lombok.Builder;
import lombok.Getter;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.ThrowableProblem;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tai.khuu on 6/21/20.
 */

@Getter
public class ProductServiceException extends AbstractThrowableProblem {

    private HttpError error;

    @Builder.Default
    private Map<String, Object> errorData = new HashMap<>();

    @Builder
    public ProductServiceException(HttpError error) {
        this.error = error;
    }

    public void addErrorData(String key, Object value) {
        errorData.put(key, value);
    }
}
