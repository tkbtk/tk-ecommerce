package com.tk.ps.errors;

/**
 * Created by tai.khuu on 6/21/20.
 */
public interface HttpError extends IError {

    int getHttpStatus();
}
