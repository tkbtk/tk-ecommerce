package com.tk.ps.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Getter
@AllArgsConstructor
public enum ProductServiceError implements HttpError {

    DEFAULT_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "ER000", "Internal server error"),
    DEFAULT_RULE_VIOLATION(HttpStatus.CONFLICT.value(), "ER500", "Invalid input data"),
    DEFAULT_AUTHENTICATION(HttpStatus.FORBIDDEN.value(), "ER501", "This request is required authentication"),
    DEFAULT_AUTHORIZATION(HttpStatus.FORBIDDEN.value(), "ER502", "This request is forbidden"),
    DEFAULT_RESOURCE_NOT_FOUND(HttpStatus.NOT_FOUND.value(), "ER503", "Required resource not found");

    private int httpStatus;

    private String errorCode;

    private String errorMessage;
}
