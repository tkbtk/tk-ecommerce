package com.tk.ps.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.zalando.problem.spring.web.advice.security.SecurityAdviceTrait;

import java.util.Optional;

@Configuration
@EnableJpaRepositories("com.tk.ps.repository")
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@EnableTransactionManagement
public class DatabaseConfiguration {

    private final Environment env;

    public DatabaseConfiguration(Environment env) {
        this.env = env;
    }

    @Bean
    public SecurityAditorAware springSecurityAuditorAware() {
        return new SecurityAditorAware();
    }

    public static class SecurityAditorAware implements AuditorAware<String> {

        @Override
        public Optional<String> getCurrentAuditor() {
            //TODO temporory set this to system, after that we will get the real user_name
            return Optional.of("system");
        }
    }
}
