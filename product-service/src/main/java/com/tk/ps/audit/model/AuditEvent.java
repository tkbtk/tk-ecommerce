package com.tk.ps.audit.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuditEvent {
    private AuditAction action;

    @Builder.Default
    private Map<String, String> payload = new HashMap<>();

    public AuditEvent addData(String key, String val) {
        payload.put(key, val);
        return this;
    }

}
