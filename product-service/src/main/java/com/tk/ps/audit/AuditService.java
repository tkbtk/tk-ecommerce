package com.tk.ps.audit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.ps.audit.model.AuditEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Slf4j
@Service
public class AuditService {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TaskExecutor taskExecutor;

    public void sendAuditEvent(AuditEvent auditEvent) {
        taskExecutor.execute(() -> {
            try {
                kafkaTemplate.send("audit-topic", "bind-this-to-parition-unique-key", objectMapper.writeValueAsString(auditEvent));
            } catch (Exception e) {
                //ignore exception
                log.error("Exception when send kafka message", e);
            }
        });
    }

}
