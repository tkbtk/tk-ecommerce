package com.tk.ps.service;

import com.tk.ps.audit.AuditService;
import com.tk.ps.audit.model.AuditAction;
import com.tk.ps.audit.model.AuditEvent;
import com.tk.ps.errors.ProductServiceError;
import com.tk.ps.errors.ProductServiceException;
import com.tk.ps.mapper.DtoMapper;
import com.tk.ps.mapper.EntityMapper;
import com.tk.ps.model.domain.Category;
import com.tk.ps.model.domain.ProductItem;
import com.tk.ps.model.domain.ProductPrice;
import com.tk.ps.model.domain.ProductPriceHistory;
import com.tk.ps.model.dto.ProductDetailsDto;
import com.tk.ps.model.dto.ProductDto;
import com.tk.ps.repository.CategoryRepository;
import com.tk.ps.repository.ProductItemRepository;
import com.tk.ps.repository.ProductPriceHistoryRepository;
import com.tk.ps.repository.ProductPriceRepository;
import com.tk.ps.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    private final ProductItemRepository productItemRepository;

    private final CategoryRepository categoryRepository;

    private final ProductPriceRepository productPriceRepository;

    private final ProductPriceHistoryRepository productPriceHistoryRepository;

    private final AuditService auditService;

    @Transactional
    public Page<ProductDto> getAllProduct(Pageable page) {
        return productRepository.getAllProduct(page).map(DtoMapper.INSTANCE::from);
    }

    @Transactional
    public Page<ProductDto> getProductByCategoryName(String categoryName, Pageable page) {
        Optional<Category> categoryByName = categoryRepository.getCategoryByName(categoryName);
        Category category = categoryByName.orElseThrow(() -> ProductServiceException.builder().error(ProductServiceError.DEFAULT_RESOURCE_NOT_FOUND).build());
        return productRepository.getProductByCategory(category.getId(), page).map(DtoMapper.INSTANCE::from);
    }

    @Transactional
    public Page<ProductDto> searchProduct(String query, Pageable page) {
        auditService.sendAuditEvent(AuditEvent.builder().action(AuditAction.USER_SEARCH).build().addData("query", query));
        return productRepository.searchProductByNameAndDescription(query, page).map(DtoMapper.INSTANCE::from);
    }


    @Transactional
    public Optional<ProductDetailsDto> getProductBySku(String sku) {
        Optional<ProductItem> productItemBySku = productItemRepository.getProductItemBySku(sku);
        return productItemBySku.map(DtoMapper.INSTANCE::toProductDetailsDto);
    }

    @Transactional
    public void updateProductPrice(Long productId, BigInteger price) {
        if (!productRepository.existsById(productId)) {
            throw ProductServiceException.builder().error(ProductServiceError.DEFAULT_RESOURCE_NOT_FOUND).build();
        }
        ProductPrice productPrice = productPriceRepository.getProductPrice(productId);
        if (price != null && !Objects.equals(price, productPrice.getPrice())) {
            ProductPriceHistory priceHistory = EntityMapper.INSTANCE.fromProductPrice(productPrice, price);
            productPriceRepository.save(productPrice);
            productPriceHistoryRepository.save(priceHistory);
        }
    }
}
