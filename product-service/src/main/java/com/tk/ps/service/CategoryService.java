package com.tk.ps.service;

import com.tk.ps.mapper.DtoMapper;
import com.tk.ps.model.dto.CategoryDto;
import com.tk.ps.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Service
@RequiredArgsConstructor
public class CategoryService {

    private CategoryRepository categoryRepository;

    @Transactional
    public Page<CategoryDto> getAllCategory(Pageable pageable) {
        return categoryRepository.getAllCategory(pageable).map(DtoMapper.INSTANCE::from);
    }

}
