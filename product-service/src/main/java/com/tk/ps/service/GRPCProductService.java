package com.tk.ps.service;

import com.tk.proto.product.GetProductItemRequest;
import com.tk.proto.product.GetProductItemResponse;
import com.tk.proto.product.ProductAttribute;
import com.tk.proto.product.ProductServiceGrpc;
import com.tk.ps.errors.ProductServiceError;
import com.tk.ps.errors.ProductServiceException;
import com.tk.ps.model.dto.ProductDetailsDto;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tai.khuu on 6/22/20.
 */
@GRpcService
public class GRPCProductService extends ProductServiceGrpc.ProductServiceImplBase {

    @Autowired
    private ProductService productService;

    @Override
    public void getProduct(GetProductItemRequest request, StreamObserver<GetProductItemResponse> responseObserver) {
        Optional<ProductDetailsDto> productBySku = productService.getProductBySku(request.getSku());
        ProductDetailsDto productDetailsDto = productBySku.orElseThrow(() -> ProductServiceException.builder().error(ProductServiceError.DEFAULT_RESOURCE_NOT_FOUND).build());
        GetProductItemResponse result = GetProductItemResponse
            .newBuilder()
            .addAllAttributes(productDetailsDto
                .getAttributes()
                .stream()
                .map(a -> ProductAttribute
                    .newBuilder()
                    .setName(a.getName())
                    .setValue(a.getValue())
                    .build()).collect(Collectors.toList()))
            .setName(productDetailsDto.getName())
            .setDescription(productDetailsDto.getDescription())
            .setPrice(productDetailsDto.getPrice().intValue())
            .setSku(productDetailsDto.getSku())
            .setInventoryCount(productDetailsDto.getInventory())
            .build();
        responseObserver.onNext(result);
        responseObserver.onCompleted();
    }
}
