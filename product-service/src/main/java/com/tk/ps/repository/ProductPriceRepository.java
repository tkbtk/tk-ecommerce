package com.tk.ps.repository;

import com.tk.ps.model.domain.ProductPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Repository
public interface ProductPriceRepository extends JpaRepository<ProductPrice, Long> {

    @Query("select pp from ProductPrice pp where pp.product.id=?1")
    ProductPrice getProductPrice(Long productId);
}
