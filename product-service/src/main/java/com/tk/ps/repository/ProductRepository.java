package com.tk.ps.repository;

import com.tk.ps.model.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by tai.khuu on 6/21/20.
 */

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("select p from Product p")
    Page<Product> getAllProduct(Pageable page);

    @Query("select p from Product p  join p.categories c where c.id =:categoryId")
    Page<Product> getProductByCategory(@Param("categoryId") long categoryId, Pageable pag);


    @Query(value = "select * from product where SIMILARITY(:query, name) > 0.2 or SIMILARITY(:query, description) > 0.2 \n-- #pageable\n",
        countQuery = "select * from product where SIMILARITY(:query, name) > 0.2 or SIMILARITY(:query, description) > 0.2", nativeQuery = true)
    Page<Product> searchProductByNameAndDescription(@Param("query") String query, Pageable pageable);
}
