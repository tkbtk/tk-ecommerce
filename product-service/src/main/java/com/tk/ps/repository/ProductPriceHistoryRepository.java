package com.tk.ps.repository;

import com.tk.ps.model.domain.ProductPriceHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Repository
public interface ProductPriceHistoryRepository extends JpaRepository<ProductPriceHistory, Long> {

    @Query("select pph from ProductPriceHistory pph where pph.product.id=?1")
    List<ProductPriceHistory> getProductPriceHistory(Long productId);
}
