package com.tk.ps.repository;

import com.tk.ps.model.domain.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Optional<Category> getCategoryByName(String categoryName);

    @Query("select c from Category c")
    Page<Category> getAllCategory(Pageable page);

}
