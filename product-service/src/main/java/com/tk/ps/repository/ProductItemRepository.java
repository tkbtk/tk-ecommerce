package com.tk.ps.repository;

import com.tk.ps.model.domain.ProductItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Repository
public interface ProductItemRepository extends JpaRepository<ProductItem, Long> {

    @Query("select p from ProductItem p where p.sku=?1")
    Optional<ProductItem> getProductItemBySku(String sku);

}
