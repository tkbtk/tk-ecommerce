package com.tk.ps.web.rest.controller;

import com.tk.ps.model.dto.CategoryDto;
import com.tk.ps.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tai.khuu on 6/21/20.
 */

@RestController
@RequestMapping(path = "/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public Page<CategoryDto> getAllCategory(Pageable page) {
        return categoryService.getAllCategory(page);
    }


}
