package com.tk.ps.web.rest.errors;

import com.tk.ps.errors.HttpError;
import com.tk.ps.errors.ProductServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.ProblemHandling;
import org.zalando.problem.spring.web.advice.security.SecurityAdviceTrait;


/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 * The error response follows RFC7807 - Problem Details for HTTP APIs (https://tools.ietf.org/html/rfc7807).
 */
@Slf4j
@ControllerAdvice
public class ProductServiceExceptionTranslator implements ProblemHandling, SecurityAdviceTrait {

    public static final String MESSAGE = "message";

    public static final String CODE = "code";

    public static final String DATA = "data";

    @ExceptionHandler
    public ResponseEntity<Problem> handlePSException(ProductServiceException ex, NativeWebRequest request) {
        HttpError error = ex.getError();
        int httpStatus = error.getHttpStatus();
        return ResponseEntity.status(httpStatus).body(Problem.builder()
            .withStatus(Status.valueOf(httpStatus))
            .withCause(ex.getCause())
            .withTitle(error.getErrorMessage())
            .with(MESSAGE, error.getErrorMessage())
            .with(CODE, error.getErrorCode())
            .with(DATA, ex.getErrorData())
            .build());
    }
}
