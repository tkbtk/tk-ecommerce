package com.tk.ps.web.rest.controller;

import com.tk.ps.model.dto.PriceUpdateRequestDto;
import com.tk.ps.model.dto.ProductDto;
import com.tk.ps.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

/**
 * Created by tai.khuu on 6/21/20.
 */
@RequiredArgsConstructor
@RestController()
@RequestMapping(path = "/product")
public class ProductController {

    private final ProductService productService;

    @GetMapping()
    public Page<ProductDto> getAllProduct(Pageable page) {
        return productService.getAllProduct(page);
    }

    @GetMapping(params = "category")
    public Page<ProductDto> getAllProductByCategory(@RequestParam("category") String categoryName, Pageable page) {
        return productService.getProductByCategoryName(categoryName, page);
    }

    @GetMapping("/search")
    public Page<ProductDto> searchProduct(@RequestParam("query") String query, Pageable page) {
        return productService.searchProduct(query, page);
    }

    @PutMapping("/{id}/price")
    public void updateProducePrice(@PathVariable("id") Long productId, @RequestBody PriceUpdateRequestDto request) {
        productService.updateProductPrice(productId, request.getNewPrice());
    }
}
