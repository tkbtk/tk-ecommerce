package com.tk.ps.mapper;

import com.tk.ps.model.domain.Category;
import com.tk.ps.model.domain.Product;
import com.tk.ps.model.domain.ProductItem;
import com.tk.ps.model.domain.ProductItemAttribute;
import com.tk.ps.model.domain.ProductPrice;
import com.tk.ps.model.dto.CategoryDto;
import com.tk.ps.model.dto.ProductDetailsDto;
import com.tk.ps.model.dto.ProductDto;
import com.tk.ps.model.dto.ProductItemAttributeDto;
import com.tk.ps.model.dto.ProductItemDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.math.BigInteger;

/**
 * Created by tai.khuu on 5/25/20.
 */
@Mapper
public interface DtoMapper {

    DtoMapper INSTANCE = Mappers.getMapper(DtoMapper.class);

    CategoryDto from(Category category);

    ProductDto from(Product product);

    default BigInteger from(ProductPrice price) {
        return price.getPrice();
    }

    ProductItemDto from(ProductItem productItem);

    @Mappings({
        @Mapping(target = "id", source = "id"),
        @Mapping(target = "name", source = "productAttribute.name"),
        @Mapping(target = "value", source = "value"),
        @Mapping(target = "createdBy", source = "createdBy"),
        @Mapping(target = "createdDate", source = "createdDate"),
        @Mapping(target = "lastModifiedBy", source = "lastModifiedBy"),
        @Mapping(target = "lastModifiedDate", source = "lastModifiedDate"),
    })
    ProductItemAttributeDto from(ProductItemAttribute attribute);

    @Mappings({
        @Mapping(target = "id", source = "id"),
        @Mapping(target = "sku", source = "sku"),
        @Mapping(target = "name", source = "product.name"),
        @Mapping(target = "description", source = "product.description"),
        @Mapping(target = "price", source = "product.price.price"),
        @Mapping(target = "attributes", source = "attributes"),
        @Mapping(target = "inventory", source = "productItemInventory.count"),
    })
    ProductDetailsDto toProductDetailsDto(ProductItem productItem);
}
