package com.tk.ps.mapper;

import com.tk.ps.model.domain.ProductPrice;
import com.tk.ps.model.domain.ProductPriceHistory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.math.BigInteger;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Mapper
public interface EntityMapper {

    EntityMapper INSTANCE = Mappers.getMapper(EntityMapper.class);

    @Mapping(target = "product", source = "productPrice.product")
    @Mapping(target = "oldPrice", source = "productPrice.price")
    @Mapping(target = "newPrice", source = "newPrice")
    ProductPriceHistory fromProductPrice(ProductPrice productPrice, BigInteger newPrice);

}
