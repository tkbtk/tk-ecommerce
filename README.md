# This is a example ecommerce application.

The application is a show case microservices that use gRPC for service to service communication.

This application contain 4 folder

local-env: Contain docker-compose file and script run the application stack in local environment.

tk-proto-repo: Contain all gRPC proto file.

product-service: Simple Spring Boot application. Use to manange, search, and get product.

order-service: Simple quarkus service to place item on shopping cart etc.

## Quick start: ##

Requirement: JDK 11, docker, docker-compose

tk-proto-repo

``` bash
❯ cd tk-proto-repo
❯ ./gradlew clean install
```

product-service

``` bash
❯ cd product-service
❯ ./gradlew jibDockerBuild

```

order-service

``` bash
❯ cd order-service
❯ ./mvnw clean package -Dquarkus.container-image.build=true
```

local-env 

``` bash
❯ cd order-service
❯ docker-compose up -d
```


Some example curl

Search

``` bash
curl --location --request GET 'localhost:8081/product/search?query=comp'
```

Get all product

``` bash
curl --location --request GET 'localhost:8081/product/search?query=comp'
```

Add item to cart 

``` bash
curl --location --request POST 'localhost:8080/shopping-cart/item' \
--header 'Content-Type: application/json' \
--data-raw '{
	"sku":"e21e7774-d05b-417c-9a9d-7af0b3f7eda3",
	"quantity":2
}'
```

Get shopping cart

``` bash
curl --location --request GET 'localhost:8080/shopping-cart'
```



## Technologies ##

### gRPC ###

gRPC is the main protocol used for service to service communication. gRPC use HTTP/2 multiplexing feature. It's very fast, together with the protobuf compressing feature. gRPC fit really well with Micro-Service eco-system.

### Spring Boot ###

Product service use Spring Boot to ease the development process

### PostgreSQL ###

PostgreSQL are used as the main database and also search method using the extention `pg_trgm`. Search method can actually be improved using ElasticSearch.

### Kafka ###

Kafka is used to store audit event from user action.

### Quarkus ###

Subatomic microservice framework, big benefit of Quarkus is that it can build native image, imagine Java application with native Boot speed and small memory foot print. Quarkus also have a very good hot reload experience.

### Google Jib ###

All the application above use Google Jib to build image, Jib is very fast since all the artifact are cache and only append to the last layer, this will make Jib image very small too. Jib doesn't require docker to build image which is also a good point.

ERD

Generated through plantuml
![Product ERD](product_erd.png)

Generated from db

![Order ERD](order_erd.png)


Deployment model

This is a very simple deployment model use Istio Service Mesh, EKS, Ingress traffic come through Application Load Balancer that is secured using AWS WAF.

This deployment model doesn't handle how to deal with Egress, VPC, Subnet ... yet.

![Deployment Model](example_deployment.png)
