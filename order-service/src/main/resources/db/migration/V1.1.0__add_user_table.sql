CREATE TABLE customer
(
    id                 SERIAL PRIMARY KEY,
    name               INTEGER     NOT NULL UNIQUE,
    first_name         VARCHAR(50) NOT NULL,
    last_name          VARCHAR(50) NOT NULL,
    email              VARCHAR(50) NOT NULL,
    created_by         VARCHAR(50) NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50) NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

