CREATE TABLE customer_order(
    id                 SERIAL PRIMARY KEY,
    status             INTEGER     NOT NULL UNIQUE,
    created_by         VARCHAR(50) NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50) NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE order_item
(
    id                 SERIAL PRIMARY KEY,
    order_id           INTEGER      NOT NULL,
    product_sku        VARCHAR(255) NOT NULL UNIQUE,
    quantity           INTEGER      NOT NULL,
    created_by         VARCHAR(50)  NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   VARCHAR(50)  NOT NULL,
    last_modified_date TIMESTAMP WITHOUT TIME ZONE
);


ALTER TABLE order_item
    ADD CONSTRAINT fk_order_item_order FOREIGN KEY (order_id) REFERENCES customer_order (id);
