package com.tk.os.model.dto;

import lombok.Data;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
public class AddItemRequestDto {

    private String sku;

    private int quantity;
}
