package com.tk.os.model.domain;

import com.tk.os.model.ThreadLocalSecurityContext;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Optional;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
@Entity
@Table(name = "order_item")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "product_sku")
    private String productSKU;


    @ManyToOne(targetEntity = Order.class, optional = false)
    @JoinColumn(name = "order_id")
    private Order order;

    @Column
    private int quantity;

    @Column
    private int price;

    @Column(name = "created_by", nullable = false, length = 50, updatable = false)
    private String createdBy;

    @Column(name = "created_date", updatable = false)
    private Instant createdDate = Instant.now();

    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate = Instant.now();

    public OrderItem() {
    }

    public OrderItem(Order order, String productSKU, int quantity) {
        this.order = order;
        this.productSKU = productSKU;
        this.quantity = quantity;
    }

    @PrePersist
    public void prePersis() {
        createdDate = Instant.now();
        createdBy = Optional.ofNullable(ThreadLocalSecurityContext.getSecurityIdentity()).map(si -> si.getPrincipal().getName()).orElse("system");
        lastModifiedBy = createdBy;
        lastModifiedDate = createdDate;
    }

    @PreUpdate
    public void preUpdate() {
        lastModifiedDate = Instant.now();
        lastModifiedBy = Optional.ofNullable(ThreadLocalSecurityContext.getSecurityIdentity()).map(si -> si.getPrincipal().getName()).orElse("system");
    }
}
