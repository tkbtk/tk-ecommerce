package com.tk.os.model.domain;

/**
 * Created by tai.khuu on 6/21/20.
 */
public enum OrderStatus {
    IN_PROGRESS, SUBMIT, CANCEL, COMPLETE, IN_TRANSIT
}
