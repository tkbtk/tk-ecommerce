package com.tk.os.model.domain;

import com.tk.os.model.ThreadLocalSecurityContext;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by tai.khuu on 6/21/20.
 */
@Data
@Entity
@Table(name = "customer_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public Order() {
    }

    public Order(OrderStatus status) {
        this.status = status;
    }

    @Column
    @Enumerated(value = EnumType.ORDINAL)
    private OrderStatus status;


    @OneToMany(targetEntity = OrderItem.class, fetch = FetchType.EAGER, mappedBy = "order")
    private List<OrderItem> items = new ArrayList<>();


    @Column(name = "created_by", nullable = false, length = 50, updatable = false)
    private String createdBy;

    @Column(name = "created_date", updatable = false)
    private Instant createdDate = Instant.now();

    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate = Instant.now();

    @PrePersist
    public void prePersis() {
        createdDate = Instant.now();
        createdBy = Optional.ofNullable(ThreadLocalSecurityContext.getSecurityIdentity()).map(si -> si.getPrincipal().getName()).orElse("system");
        lastModifiedBy = createdBy;
        lastModifiedDate = createdDate;
    }

    @PreUpdate
    public void preUpdate() {
        lastModifiedDate = Instant.now();
        lastModifiedBy = Optional.ofNullable(ThreadLocalSecurityContext.getSecurityIdentity()).map(si -> si.getPrincipal().getName()).orElse("system");
    }
}
