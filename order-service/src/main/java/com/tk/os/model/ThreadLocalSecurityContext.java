package com.tk.os.model;

import io.quarkus.security.identity.SecurityIdentity;

/**
 * Created by tai.khuu on 6/21/20.
 */
public class ThreadLocalSecurityContext {

    private static final ThreadLocal<SecurityIdentity> IDENT = new ThreadLocal<>();

    public static SecurityIdentity getSecurityIdentity() {
        return IDENT.get();
    }

    public static void putSecurityIdentity(SecurityIdentity identity) {
        IDENT.set(identity);
    }

    public static void clearSecurityIdentity() {
        IDENT.remove();
    }
}
