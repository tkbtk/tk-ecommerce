package com.tk.os.model.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Data
public class OrderLineDto {

    private String sku;

    private int quantity;

    private BigDecimal total;

    private BigDecimal price;
}
