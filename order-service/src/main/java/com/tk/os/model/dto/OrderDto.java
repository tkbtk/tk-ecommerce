package com.tk.os.model.dto;

import com.tk.os.model.domain.OrderStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Data
public class OrderDto {

    private long id;

    private OrderStatus status;

    private List<OrderLineDto> items;

    private BigDecimal total;
}
