package com.tk.os.error;

/**
 * Created by tai.khuu on 6/22/20.
 */
public interface IError {

    String getErrorCode();

    String getErrorMessage();

}
