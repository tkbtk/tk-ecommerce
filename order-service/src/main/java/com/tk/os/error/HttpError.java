package com.tk.os.error;

import org.zalando.problem.Status;

/**
 * Created by tai.khuu on 6/22/20.
 */

public interface HttpError extends IError{
    Status getStatus();
}
