package com.tk.os.error;

import lombok.Builder;
import lombok.Data;
import org.zalando.problem.AbstractThrowableProblem;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Data
public class OrderServiceException extends AbstractThrowableProblem {

    private HttpError error;

    @Builder
    public OrderServiceException(HttpError error) {
        this.error = error;
    }
}
