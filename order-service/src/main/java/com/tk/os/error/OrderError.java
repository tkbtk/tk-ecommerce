package com.tk.os.error;

import lombok.Getter;
import org.zalando.problem.Status;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Getter
public enum OrderError implements HttpError {

    DEFAULT_ERROR(Status.INTERNAL_SERVER_ERROR, "ER000", "Internal server error"),
    DEFAULT_RULE_VIOLATION(Status.CONFLICT, "ER500", "Invalid input data"),
    DEFAULT_AUTHENTICATION(Status.UNAUTHORIZED, "ER501", "This request is required authentication"),
    DEFAULT_AUTHORIZATION(Status.FORBIDDEN, "ER502", "This request is forbidden"),
    DEFAULT_RESOURCE_NOT_FOUND(Status.NOT_FOUND, "ER503", "Required resource not found");

    private Status status;

    private String errorCode;

    private String errorMessage;

    private static final String PREFIX_CODE = "OS";

    OrderError(Status status, String errorCode, String errorMessage) {
        this.status = status;
        this.errorCode = PREFIX_CODE + errorCode;
        this.errorMessage = errorMessage;
    }
}
