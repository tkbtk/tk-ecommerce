package com.tk.os.mapper;

import com.tk.os.model.domain.Order;
import com.tk.os.model.domain.OrderItem;
import com.tk.os.model.dto.OrderDto;
import com.tk.os.model.dto.OrderLineDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Mapper
public interface DtoMapper {

    DtoMapper INSTANCE = Mappers.getMapper(DtoMapper.class);

    @Mapping(target = "status", source = "status")
    @Mapping(target = "items", source = "items")
    @Mapping(target = "id", source = "id")
    @Mapping(target = "total", source = "items", qualifiedByName = "totalAmount")
    OrderDto from(Order order);


    @Mapping(target = "sku", source = "productSKU")
    @Mapping(target = "quantity", source = "quantity")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "total", source = ".", qualifiedByName = "totalAmountFromQuantity")
    OrderLineDto from(OrderItem item);

    List<OrderLineDto> from(List<OrderItem> item);

    @Named("totalAmountFromQuantity")
    default BigDecimal getTotalFromQuantity(OrderItem orderItem) {
        BigDecimal bigDecimal = BigDecimal.valueOf(orderItem.getPrice());
        return bigDecimal.multiply(BigDecimal.valueOf(orderItem.getQuantity()));
    }

    @Named("totalAmount")
    default BigDecimal totalAmount(List<OrderItem> orderItems) {
        return orderItems.stream().map(this::getTotalFromQuantity).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

}
