package com.tk.os.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.jackson.ObjectMapperCustomizer;
import org.zalando.problem.ProblemModule;

import javax.inject.Singleton;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Singleton
public class RegisterCustomModuleCustomizer implements ObjectMapperCustomizer {

    @Override
    public void customize(ObjectMapper objectMapper) {
        objectMapper.registerModule(new ProblemModule());
    }
}
