package com.tk.os.service;

import com.tk.os.error.OrderError;
import com.tk.os.error.OrderServiceException;
import com.tk.os.mapper.DtoMapper;
import com.tk.os.model.domain.Order;
import com.tk.os.model.domain.OrderItem;
import com.tk.os.model.domain.OrderStatus;
import com.tk.os.model.dto.AddItemRequestDto;
import com.tk.os.model.dto.OrderDto;
import com.tk.os.repository.OrderItemRepository;
import com.tk.os.repository.OrderRepository;
import com.tk.proto.product.GetProductItemRequest;
import com.tk.proto.product.GetProductItemResponse;
import com.tk.proto.product.ProductServiceGrpc;
import io.quarkus.grpc.runtime.annotations.GrpcService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by tai.khuu on 6/21/20.
 */
@ApplicationScoped
public class OrderService {

    @Inject
    private OrderRepository orderRepository;

    @Inject
    private OrderItemRepository orderItemRepository;

    @Inject
    @GrpcService("product")
    private ProductServiceGrpc.ProductServiceBlockingStub productService;

    @Transactional
    public void addNewItem(AddItemRequestDto addItemRequestDto) {
        Optional<Order> orderOptional = orderRepository.findOrderByStatus(OrderStatus.IN_PROGRESS);
        Order order = orderOptional.orElse(new Order(OrderStatus.IN_PROGRESS));
        GetProductItemResponse product = productService.
                getProduct(GetProductItemRequest.newBuilder()
                        .setSku(addItemRequestDto.getSku()).build());
        OrderItem orderItem = orderItemRepository.findBySku(addItemRequestDto.getSku()).orElse(new OrderItem(order, addItemRequestDto.getSku(), 0));
        orderItem.setQuantity(orderItem.getQuantity() + addItemRequestDto.getQuantity());
        orderItem.setPrice(product.getPrice());
        order.getItems().add(orderItem);
        orderRepository.persist(order);
        orderItemRepository.persist(orderItem);
    }

    @Transactional
    public void removeItem(long itemId) {
        Optional<OrderItem> optional = orderItemRepository.findByIdOptional(itemId);
        OrderItem orderItem = optional.orElseThrow(() -> OrderServiceException.builder().error(OrderError.DEFAULT_RESOURCE_NOT_FOUND).build());
        orderItem.setQuantity(Math.max(orderItem.getQuantity() - 1, 0));
        if (orderItem.getQuantity() == 0) {
            orderItemRepository.delete(orderItem);
        } else {
            orderItemRepository.persist(orderItem);
        }
    }

    @Transactional
    public OrderDto getShoppingCart() {
        Optional<Order> orderByStatus = orderRepository.findOrderByStatus(OrderStatus.IN_PROGRESS);
        Order order = orderByStatus.orElseGet(() -> {
            Order entity = new Order(OrderStatus.IN_PROGRESS);
            orderRepository.persist(entity);
            return entity;
        });
        syncProductData(order);
        return DtoMapper.INSTANCE.from(order);
    }

    @Transactional
    public void syncProductData(Order order) {
        List<OrderItem> orderItems = order.getItems();
        for (OrderItem orderItem : orderItems) {
            GetProductItemResponse product = productService.getProduct(GetProductItemRequest.newBuilder().setSku(orderItem.getProductSKU()).build());
            orderItem.setPrice(product.getPrice());
            orderItemRepository.persist(orderItem);
        }
    }
}
