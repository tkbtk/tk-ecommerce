package com.tk.os.repository;

import com.tk.os.model.domain.OrderItem;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

/**
 * Created by tai.khuu on 6/21/20.
 */
@ApplicationScoped
public class OrderItemRepository implements PanacheRepository<OrderItem> {

    public Optional<OrderItem> findBySku(String sku) {
        PanacheQuery<OrderItem> query = find("select i from OrderItem i where i.productSKU=?1", sku);
        return query.singleResultOptional();
    }

}
