package com.tk.os.repository;

import com.tk.os.model.domain.Order;
import com.tk.os.model.domain.OrderStatus;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

/**
 * Created by tai.khuu on 6/21/20.
 */
@ApplicationScoped
public class OrderRepository implements PanacheRepository<Order> {

    public Optional<Order> findOrderByStatus(OrderStatus status) {
        PanacheQuery<Order> orderPanacheQuery = find("select o from Order o where o.status = ?1", status);
        return orderPanacheQuery.singleResultOptional();
    }
}
