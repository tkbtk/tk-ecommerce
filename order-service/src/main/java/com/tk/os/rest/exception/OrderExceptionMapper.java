package com.tk.os.rest.exception;

import com.tk.os.error.HttpError;
import com.tk.os.error.OrderServiceException;
import org.zalando.problem.Problem;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by tai.khuu on 6/22/20.
 */
@Provider
public class OrderExceptionMapper implements ExceptionMapper<OrderServiceException> {

    @Override
    public Response toResponse(OrderServiceException exception) {
        HttpError error = exception.getError();
        return Response.status(error.getStatus().getStatusCode())
                .entity(Problem.builder().with("code",
                        error.getErrorCode())
                        .with("message", error.getErrorMessage())
                        .withTitle(error.getErrorMessage())
                        .withStatus(error.getStatus()).build()).build();
    }
}
