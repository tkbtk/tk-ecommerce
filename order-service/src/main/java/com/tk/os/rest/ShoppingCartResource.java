package com.tk.os.rest;

import com.tk.os.model.dto.AddItemRequestDto;
import com.tk.os.model.dto.OrderDto;
import com.tk.os.service.OrderService;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/shopping-cart")
public class ShoppingCartResource {

    @Inject
    private OrderService orderService;

    @POST()
    @Path("/item")
    @Produces(MediaType.TEXT_PLAIN)
    public void addItemToCart(AddItemRequestDto request) {
        orderService.addNewItem(request);
    }

    @GET()
    @Produces(MediaType.APPLICATION_JSON)
    public OrderDto getCurrentShoppingCart() {
        return orderService.getShoppingCart();
    }

    @DELETE
    @Path("/item/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteItem(@PathParam("id") long id) {
        orderService.removeItem(id);
    }
}